//
//  main.m
//  groupieFace
//
//  Created by Jay Vaughan on 6/28/16.
//  Copyright © 2016 Jay Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
