#Set these up here, instead of having to remember to do it beforehand..
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/lib:$(shell brew --prefix opencv3)/lib/pkgconfig
export CPPFLAGS=$(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config --cflags opencv)
export CPPLIBS=$(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config --libs opencv)

groupie:	groupie.cpp haarcascade_frontalface_alt.xml
	clang++  -stdlib=libc++ -o groupie `pkg-config --cflags opencv tinyxml2` groupie.cpp  `pkg-config --libs opencv tinyxml2` -lboost_filesystem-mt  -lboost_system-mt

indent:	groupie.cpp
	gindent -kr -ts4 groupie.cpp

os: groupie.cpp haarcascade_frontalface_alt.xml
	clang++  -stdlib=libc++  -framework IOKit -framework Cocoa `pkg-config --cflags opencv` groupie.cpp  `pkg-config --libs opencv` -lboost_filesystem-mt  -lboost_system-mt -o groupie
clean:
	rm -rf groupie *.o

setup:
	#brew tap homebrew/science && 
	brew install opencv3 tinyxml2 boost



